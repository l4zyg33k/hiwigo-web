insert into vehicle(id, number, driver, mobile) values (1, '43 가 7723', '홍길동', '010-2346-3457');
insert into vehicle(id, number, driver, mobile) values (2, '33 바 7834', '변사또', '010-9747-4473');
insert into vehicle(id, number, driver, mobile) values (3, '24 파 4454', '성춘향', '010-3242-3234');
insert into vehicle(id, number, driver, mobile) values (4, '23 나 3243', '이몽룡', '010-2375-0434');

insert into route(id, name, vehicle_id) values (1, '강남구 배송', 1);

insert into route_pin(id, name, address, turn, longitude, latitude, route_id ) values (1, 'GS타워', '서울특별시 강남구 역삼1동 논현로 508', 1, 127.037130, 37.502027, 1);
insert into route_pin(id, name, address, turn, longitude, latitude, route_id ) values (2, '강남파이낸스센터', '서울특별시 강남구 역삼동 737', 2, 127.036489, 37.500201, 1);
insert into route_pin(id, name, address, turn, longitude, latitude, route_id ) values (3, '역삼1동우체국', '서울특별시 강남구 역삼동 647-9', 3, 127.033103, 37.500414, 1);
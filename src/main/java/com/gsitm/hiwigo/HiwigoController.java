package com.gsitm.hiwigo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HiwigoController {

	@GetMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("/views/{view}.html")
	public String views(@PathVariable(value = "view") String view) {
		return String.format("views/%s", view);
	}

	@GetMapping("/views/{category}/{view}.html")
	public String views(@PathVariable(value = "category") String category, @PathVariable(value = "view") String view) {
		return String.format("views/%s/%s", category, view);
	}
}

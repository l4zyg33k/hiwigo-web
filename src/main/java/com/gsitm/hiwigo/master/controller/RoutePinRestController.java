package com.gsitm.hiwigo.master.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.RoutePin;
import com.gsitm.hiwigo.master.service.RoutePinService;

@RestController
public class RoutePinRestController {

	@Autowired
	private RoutePinService service;

	@JsonView(DataTablesOutput.View.class)
	@GetMapping("/rest/master/routepins")
	public DataTablesOutput<RoutePin> findAll(@Valid DataTablesInput input) {
		return service.findAll(input);
	}
	
	@GetMapping("/rest/master/route/{route}/coordinates")
	public String findAll(@PathVariable("route") Route route) {
		return service.getCoordinates(route);
	}	

	@GetMapping("/api/master/routepins")
	public List<RoutePin> findAll() {
		return service.findAll();
	}
	
	@GetMapping("/api/master/route/{route}/routepins")
	public List<RoutePin> findByRoute(@PathVariable("route") Route route) {
		return service.findByRoute(route);
	}	
}

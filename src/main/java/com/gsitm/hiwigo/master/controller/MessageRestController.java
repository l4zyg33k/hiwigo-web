package com.gsitm.hiwigo.master.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.hiwigo.master.model.EventLog;
import com.gsitm.hiwigo.master.model.RoutePin;
import com.gsitm.hiwigo.master.service.EventLogService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MessageRestController {

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Autowired
	private EventLogService service;

	@GetMapping("/api/message/departure/routepin/{routepin}")
	public ResponseEntity<?> departure(@PathVariable("routepin") RoutePin routepin) {
		log.debug(routepin.toString());
		EventLog log = new EventLog();
		log.setSubject("출발보고");
		log.setText(String.format("%s 차량이 %s 로 출발 하였습니다.", routepin.getRoute().getVehicle().getNumber(),
				routepin.getName()));
		log.setReceived(Calendar.getInstance().getTime());
		log = service.save(log);
		simpMessagingTemplate.convertAndSend("/topic/messages", log);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/api/message/arrival/routepin/{routepin}")
	public ResponseEntity<?> arrival(@PathVariable("routepin") RoutePin routepin) {
		log.debug(routepin.toString());
		EventLog log = new EventLog();
		log.setSubject("도착보고");
		log.setText(String.format("%s 차량이 %s 에 도착 하였습니다.", routepin.getRoute().getVehicle().getNumber(),
				routepin.getName()));
		log.setReceived(Calendar.getInstance().getTime());
		log = service.save(log);
		simpMessagingTemplate.convertAndSend("/topic/messages", log);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@SendTo("/topic/messages")
	public EventLog response(EventLog log) throws Exception {
		Thread.sleep(2000); // simulated delay
		return log;
	}
}

package com.gsitm.hiwigo.master.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.gsitm.hiwigo.master.model.Vehicle;
import com.gsitm.hiwigo.master.service.VehicleService;

@RestController
public class VehicleRestController {

	@Autowired
	private VehicleService service;

	@JsonView(DataTablesOutput.View.class)
	@GetMapping("/rest/master/vehicles")
	public DataTablesOutput<Vehicle> findAll(@Valid DataTablesInput input) {
		return service.findAll(input);
	}

	@GetMapping("/api/master/vehicles")
	public List<Vehicle> findAll() {
		return service.findAll();
	}
}

package com.gsitm.hiwigo.master.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.gsitm.hiwigo.master.model.EventLog;
import com.gsitm.hiwigo.master.service.EventLogService;

import be.ceau.chart.DoughnutChart;
import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.DoughnutData;
import be.ceau.chart.data.LineData;
import be.ceau.chart.dataset.DoughnutDataset;
import be.ceau.chart.dataset.LineDataset;

@RestController
public class DashboardRestController {
	
	@Autowired
	private EventLogService logService;

	@GetMapping("/rest/dashboard/daily/vehicles")
	public LineChart getDailyVehicles() {
		LineDataset dataset = new LineDataset().setLabel("배송 차량 대수").setData(17, 18, 16, 15, 18, 22, 19)
				.setBorderColor(new Color(0, 0, 255, 0.5)).setBackgroundColor(new Color(0, 0, 255, 0.2))
				.setBorderWidth(2).setSteppedLine(true);

		LineData data = new LineData().addLabels(LocalDate.now().minusDays(6).toString(),
				LocalDate.now().minusDays(5).toString(), LocalDate.now().minusDays(4).toString(),
				LocalDate.now().minusDays(3).toString(), LocalDate.now().minusDays(2).toString(),
				LocalDate.now().minusDays(1).toString(), LocalDate.now().toString()).addDataset(dataset);

		return new LineChart(data);
	}

	@GetMapping("/rest/dashboard/daily/stores")
	public LineChart getDailyStores() {
		LineDataset dataset = new LineDataset().setLabel("배송 점포 개수").setData(269, 271, 270, 268, 269, 271, 270)
				.setBorderColor(new Color(255, 0, 0, 0.5)).setBackgroundColor(new Color(255, 0, 0, 0.2))
				.setBorderWidth(2).setSteppedLine(true);

		LineData data = new LineData().addLabels(LocalDate.now().minusDays(6).toString(),
				LocalDate.now().minusDays(5).toString(), LocalDate.now().minusDays(4).toString(),
				LocalDate.now().minusDays(3).toString(), LocalDate.now().minusDays(2).toString(),
				LocalDate.now().minusDays(1).toString(), LocalDate.now().toString()).addDataset(dataset);

		return new LineChart(data);
	}

	@GetMapping("/rest/dashboard/daily/arriverates")
	public LineChart getDailyArriveRates() {
		LineDataset dataset = new LineDataset().setLabel("점포 정시 도착 비율")
				.setData(99.2, 98.3, 97.8, 99.4, 95.3, 96.4, 99.9).setBorderColor(new Color(0, 255, 0, 0.5))
				.setBackgroundColor(new Color(0, 255, 0, 0.2)).setBorderWidth(2).setSteppedLine(true);

		LineData data = new LineData().addLabels(LocalDate.now().minusDays(6).toString(),
				LocalDate.now().minusDays(5).toString(), LocalDate.now().minusDays(4).toString(),
				LocalDate.now().minusDays(3).toString(), LocalDate.now().minusDays(2).toString(),
				LocalDate.now().minusDays(1).toString(), LocalDate.now().toString()).addDataset(dataset);

		return new LineChart(data);
	}

	@GetMapping("/rest/dashboard/resons")
	public DoughnutChart getResons() {
		DoughnutDataset dataset = new DoughnutDataset().setLabel("점포 정시 도착 비율").setData(66.2, 26.4, 18.4)
				.addBackgroundColors(new Color(0, 255, 0, 0.2), new Color(255, 0, 0, 0.2), new Color(0, 0, 255, 0.2))
				.addBorderColors(new Color(0, 255, 0, 0.5), new Color(255, 0, 0, 0.5), new Color(0, 0, 255, 0.5))
				.setBorderWidth(2);

		DoughnutData data = new DoughnutData().addLabels("교통체증", "사전공지", "기타").addDataset(dataset);

		return new DoughnutChart(data);
	}

	@JsonView(DataTablesOutput.View.class)
	@GetMapping("/rest/dashboard/eventlogs")
	public DataTablesOutput<EventLog> findAll(@Valid DataTablesInput input) {
		return logService.findAll(input);
	}
}

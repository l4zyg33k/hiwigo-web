package com.gsitm.hiwigo.master.controller;

import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.Vehicle;
import com.gsitm.hiwigo.master.service.RouteService;

@RestController
public class RouteRestController {

	@Autowired
	private RouteService service;

	@JsonView(DataTablesOutput.View.class)
	@GetMapping("/rest/master/vehicle/{vehicle}/routes")
	public DataTablesOutput<Route> findAll(@Valid DataTablesInput input, @PathParam("vehicle") Vehicle vehicle) {
		return service.findAll(input, vehicle);
	}
	
	@GetMapping("/api/master/vehicle/{vehicle}/routes")
	public List<Route> findAll(@PathParam("vehicle") Vehicle vehicle) {
		return service.findAll(vehicle);
	}
}

package com.gsitm.hiwigo.master.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;
import org.springframework.stereotype.Repository;

import com.gsitm.hiwigo.master.model.Route;

@Repository
public interface RouteRepository extends QDataTablesRepository<Route, Long> {

}

package com.gsitm.hiwigo.master.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;

import com.gsitm.hiwigo.master.model.EventLog;

public interface EventLogRepository extends QDataTablesRepository<EventLog, Long> {

}

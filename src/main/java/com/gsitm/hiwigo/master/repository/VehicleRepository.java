package com.gsitm.hiwigo.master.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;
import org.springframework.stereotype.Repository;

import com.gsitm.hiwigo.master.model.Vehicle;

@Repository
public interface VehicleRepository extends QDataTablesRepository<Vehicle, Long> {

}

package com.gsitm.hiwigo.master.model;

import lombok.Data;

@Data
public class EndpointMessage {

	private long id;

	private String subject;

	private String text;
}

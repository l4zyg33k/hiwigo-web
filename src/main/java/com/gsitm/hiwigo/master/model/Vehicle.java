package com.gsitm.hiwigo.master.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Entity
@Data
public class Vehicle {

	@Id
	@JsonView(DataTablesOutput.View.class)
	private Long id;
	
	@JsonView(DataTablesOutput.View.class)
	private String number;	

	@JsonView(DataTablesOutput.View.class)
	private String driver;

	@JsonView(DataTablesOutput.View.class)
	private String mobile;
}

package com.gsitm.hiwigo.master.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Entity
@Data
public class EventLog {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(DataTablesOutput.View.class)
	private long id;

	@JsonView(DataTablesOutput.View.class)
	private String subject;

	@JsonView(DataTablesOutput.View.class)
	private String text;
	
	@JsonView(DataTablesOutput.View.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date received;
}

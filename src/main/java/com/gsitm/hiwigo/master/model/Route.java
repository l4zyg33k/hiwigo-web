package com.gsitm.hiwigo.master.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Entity
@Data
public class Route {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(DataTablesOutput.View.class)
	private Long id;

	@JsonView(DataTablesOutput.View.class)
	private String name;

	@ManyToOne
	@JsonView(DataTablesOutput.View.class)	
	private Vehicle vehicle;
}

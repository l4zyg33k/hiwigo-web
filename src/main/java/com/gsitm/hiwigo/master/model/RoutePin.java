package com.gsitm.hiwigo.master.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Entity
@Data
public class RoutePin {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(DataTablesOutput.View.class)
	private Long id;

	@JsonView(DataTablesOutput.View.class)
	private String name;
	
	@JsonView(DataTablesOutput.View.class)
	private String address;
	
	@JsonView(DataTablesOutput.View.class)
	private Integer turn;

	@JsonView(DataTablesOutput.View.class)
	@Column(precision = 9, scale = 6)
	private BigDecimal longitude;

	@JsonView(DataTablesOutput.View.class)
	@Column(precision = 9, scale = 6)
	private BigDecimal latitude;
	
	@JsonView(DataTablesOutput.View.class)
	@Temporal(TemporalType.TIME)
	private Date arrival;
	
	@ManyToOne
	@JsonView(DataTablesOutput.View.class)
	private Route route;	
}

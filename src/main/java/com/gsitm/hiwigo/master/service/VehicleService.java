package com.gsitm.hiwigo.master.service;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.gsitm.hiwigo.master.model.Vehicle;

public interface VehicleService {

	public DataTablesOutput<Vehicle> findAll(DataTablesInput input);
	
	public List<Vehicle> findAll();
}

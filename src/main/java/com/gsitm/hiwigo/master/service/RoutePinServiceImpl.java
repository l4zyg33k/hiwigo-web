package com.gsitm.hiwigo.master.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.gsitm.hiwigo.master.model.QRoute;
import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.RoutePin;
import com.gsitm.hiwigo.master.repository.RoutePinRepository;

@Service
public class RoutePinServiceImpl implements RoutePinService {

	@Autowired
	private RoutePinRepository repository;

	@Override
	public DataTablesOutput<RoutePin> findAll(DataTablesInput input) {
		return repository.findAll(input);
	}

	@Override
	public List<RoutePin> findAll() {
		return Lists.newArrayList(repository.findAll());
	}

	@Override
	public List<RoutePin> findByRoute(Route route) {
		QRoute qRoute = QRoute.route;
		return IteratorUtils.toList(repository.findAll(qRoute.eq(route)).iterator());
	}

	@Override
	public String getCoordinates(Route route) {
		QRoute qRoute = QRoute.route;
		Iterable<RoutePin> pins = repository.findAll(qRoute.eq(route), new Sort(Sort.Direction.ASC, "turn"));
		List<String> coors = new ArrayList<>();
		for (RoutePin pin : pins) {
			coors.add(String.format("[%s, %s]", pin.getLongitude(), pin.getLatitude()));
		}
		return String.format("[ %s ]", StringUtils.join(coors, ","));
	}

	@Override
	public RoutePin findOne(Long id) {
		return repository.findOne(id);
	}
}

package com.gsitm.hiwigo.master.service;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.gsitm.hiwigo.master.model.EventLog;

public interface EventLogService {

	public DataTablesOutput<EventLog> findAll(DataTablesInput input);
	
	public EventLog save(EventLog eventLog);
}

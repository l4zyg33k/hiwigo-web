package com.gsitm.hiwigo.master.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.hiwigo.master.model.EventLog;
import com.gsitm.hiwigo.master.repository.EventLogRepository;

@Service
public class EventLogServiceImpl implements EventLogService {

	@Autowired
	private EventLogRepository repository;

	@Override
	public DataTablesOutput<EventLog> findAll(DataTablesInput input) {
		return repository.findAll(input);
	}

	@Transactional
	@Override
	public EventLog save(EventLog eventLog) {
		return repository.save(eventLog);
	}
}

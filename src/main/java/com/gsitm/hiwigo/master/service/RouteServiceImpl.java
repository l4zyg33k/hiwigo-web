package com.gsitm.hiwigo.master.service;

import java.util.List;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import com.gsitm.hiwigo.master.model.QVehicle;
import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.Vehicle;
import com.gsitm.hiwigo.master.repository.RouteRepository;

@Service
public class RouteServiceImpl implements RouteService {

	@Autowired
	private RouteRepository repository;

	@Override
	public DataTablesOutput<Route> findAll(DataTablesInput input, Vehicle vehicle) {
		QVehicle qVehicle = QVehicle.vehicle;
		return repository.findAll(input, qVehicle.eq(vehicle));
	}

	@Override
	public List<Route> findAll(Vehicle vehicle) {
		QVehicle qVehicle = QVehicle.vehicle;
		return IteratorUtils.toList(repository.findAll(qVehicle.eq(vehicle)).iterator());
	}
}

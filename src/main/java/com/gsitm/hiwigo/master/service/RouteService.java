package com.gsitm.hiwigo.master.service;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.Vehicle;

public interface RouteService {

	public DataTablesOutput<Route> findAll(DataTablesInput input, Vehicle vehicle);

	public List<Route> findAll(Vehicle vehicle);
}

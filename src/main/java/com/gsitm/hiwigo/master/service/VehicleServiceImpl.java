package com.gsitm.hiwigo.master.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.gsitm.hiwigo.master.model.Vehicle;
import com.gsitm.hiwigo.master.repository.VehicleRepository;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepository repository;

	@Override
	public DataTablesOutput<Vehicle> findAll(DataTablesInput input) {
		return repository.findAll(input);
	}

	@Override
	public List<Vehicle> findAll() {
		return Lists.newArrayList(repository.findAll());
	}
}

package com.gsitm.hiwigo.master.service;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.gsitm.hiwigo.master.model.Route;
import com.gsitm.hiwigo.master.model.RoutePin;

public interface RoutePinService {

	public DataTablesOutput<RoutePin> findAll(DataTablesInput input);
	
	public List<RoutePin> findAll();

	public List<RoutePin> findByRoute(Route route);

	public String getCoordinates(Route route);
	
	public RoutePin findOne(Long id);
}

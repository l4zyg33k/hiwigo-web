$(function() {
	'use strict';
	
    $("#connect").click(function() {
		connect();
	});
    
	$("#disconnect").click(function() {
		disconnect();
	});

	$.get('/rest/dashboard/daily/vehicles', function(data) {
		new Chart($('#vehicles'), data);
	});
	
	$.get('/rest/dashboard/daily/stores', function(data) {
		new Chart($('#stores'), data);
	});		
	
	$.get('/rest/dashboard/daily/arriverates', function(data) {
		new Chart($('#arrives'), data);
	});	
	
	$.get('/rest/dashboard/resons', function(data) {
		new Chart($('#resons'), data);
	});
	
	var table = $('#datatable').DataTable({
		dom : '<"container"<"row"<"col-sm-6"><"col-sm-6"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7"p><"col-sm-5"i>>>',
		select : true,
		language : {
			url : 'https://cdn.datatables.net/plug-ins/1.10.13/i18n/Korean.json'
		},
		ajax : '/rest/dashboard/eventlogs',
		serverSide : true,
		rowId : 'id',
		columns : [ {
			data : 'id',
			width : '10%'
		}, {
			data : 'subject',
			width : '10%'
		}, {
			data : 'text',
			width : '40%'
		}, {
			data : 'received',
			width : '20%'
		} ],
		order: [ [ 0, "desc" ] ]
	});	
});

var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        
    }
    else {
        
    }
}

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages', function (event) {
        	$('#datatable').DataTable().ajax.reload();
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}
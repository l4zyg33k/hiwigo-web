$(function() {
	'use strict';

	var table = $('#datatable').DataTable({
		dom : '<"container"<"row"<"col-sm-6"><"col-sm-6"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7"p><"col-sm-5"i>>>',
		select : true,
		language : {
			url : 'https://cdn.datatables.net/plug-ins/1.10.13/i18n/Korean.json'
		},
		ajax : '/rest/master/routepins',
		serverSide : true,
		rowId : 'id',
		columns : [ {
			data : 'id'
		}, {
			data : 'route.name'
		}, {
			data : 'route.vehicle.number'
		}, {
			data : 'route.vehicle.driver'
		}, {
			data : 'turn'
		}, {
			data : 'name'
		}, {
			data : 'address'
		}, {
			data : 'latitude',
			visible : false
		}, {
			data : 'longitude',
			visible : false
		} ],
		order: [ [ 1, "asc" ], [ 4, "asc" ] ]
	});
	
	var longitude, latitude;
	
	var iconFeature = new ol.Feature({
		geometry : new ol.geom.Point(ol.proj.fromLonLat([ longitude, latitude ], 'EPSG:4326'))
	});

	iconFeature.setStyle(new ol.style.Style({
		image : new ol.style.Icon({
			opacity : 0.75,
			src : '/img/pin.png'
		})
	}));

	var vectorSource = new ol.source.Vector({
		features : [ iconFeature ]
	});

	var vectorLayer = new ol.layer.Vector({
		source : vectorSource
	});	

	var map = new ol.Map({
		layers : [ new ol.layer.Tile({
			source : new ol.source.OSM()
		}),  vectorLayer],
		target : 'map',
		view : new ol.View({
			projection : 'EPSG:4326',
			center : [ longitude, latitude ],
			zoom : 16
		})
	});

	$('#datatable tbody').on('click', 'tr', function() {
		var data = table.row(this).data();
		iconFeature.setGeometry(new ol.geom.Point(ol.proj.fromLonLat([ data.longitude, data.latitude ], 'EPSG:4326')));
		map.getView().setCenter([ data.longitude, data.latitude ]);
	});	
});

$(function() {
	'use strict';

	var table = $('#datatable').DataTable({
		dom : '<"container"<"row"<"col-sm-6"><"col-sm-6"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7"p><"col-sm-5"i>>>',
		select : true,
		language : {
			url : 'https://cdn.datatables.net/plug-ins/1.10.13/i18n/Korean.json'
		},
		ajax : '/rest/master/vehicles',
		serverSide : true,
		pageLength : 12,
		rowId : 'id',
		columns : [ {
			data : 'id'
		}, {
			data : 'number'
		}, {
			data : 'driver'
		}, {
			data : 'mobile'
		} ]
	});
	
	var table2 = $('#datatable2').DataTable({
		dom : '<"container"<"row"<"col-sm-6"><"col-sm-6"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-7"p><"col-sm-5"i>>>',
		select : true,
		language : {
			url : 'https://cdn.datatables.net/plug-ins/1.10.13/i18n/Korean.json'
		},
		deferLoading : 0,
		serverSide : true,
		rowId : 'id',
		columns : [ {
			data : 'id'
		}, {
			data : 'name'
		} ]
	});	
	
	$('#datatable tbody').on(
		'click',
		'tr',
		function() {
			var data = table.row(this).data();
			$('#datatable2').DataTable().ajax.url("/rest/master/vehicle/" + data.id + "/routes").load(function(json) {
				table2.row(':eq(0)', { page: 'current' }).select();
				$('#datatable2 tbody tr:eq(0)').click();
			});
	});

	var longitude, latitude;
	
	var lineFeature = new ol.Feature({
		geometry : new ol.geom.LineString([], 'XY')
	});

	var vectorSource = new ol.source.Vector({
		features : [ lineFeature ]
	});

	var vectorLayer = new ol.layer.Vector({
		source : vectorSource,
		style : [ new ol.style.Style({
			stroke : new ol.style.Stroke({
	            color : 'rgba(255, 0, 0, 0.5)',
	            width : 3
            })
		}), new ol.style.Style({
			/*
			image : new ol.style.Icon({
				opacity : 0.75,
				src : '/img/pin.png'
			}),
			*/
			image : new ol.style.Circle({
				radius : 4,
				fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 0.7)'
                })
            }),
            geometry: function(feature) {
                var coordinates = feature.getGeometry().getCoordinates();
                return new ol.geom.MultiPoint(coordinates);
            }
		}) ]
	});	

	var map = new ol.Map({
		layers : [ new ol.layer.Tile({
			source : new ol.source.OSM()
		}), vectorLayer],
		target : 'map',
		view : new ol.View({
			projection : 'EPSG:4326',
			center : [ longitude,  latitude ],
			zoom : 14
		})
	});	
	
	$('#datatable2 tbody').on('click', 'tr', function() {
		var data = table2.row(this).data();
		$.get('/rest/master/route/' + data.id + '/coordinates', function(data) {
			var markers = eval(data);
			lineFeature.setGeometry(new ol.geom.LineString(markers, 'XY'));
			map.getView().setCenter([ markers[0][0], markers[0][1] ]);
		});			
	});
});
